# Create a service oriented architecture (SOA) server application with Typescript and TSyringe [Tutorial]
This is the source code for tutorial on [Medium](https://medium.com/@cfstcyr/create-a-service-oriented-architecture-soa-server-application-with-typescript-and-tsyringe-b246623c1535)

## Steps
Each steps are marked with tags :
- [Step 1](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-1) : Installing packages
- [Step 2](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-2) : Initiate Typescript
- [Step 3](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-3) : Adding scripts to package.json
- [Step 4](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-4) : Setting up our file architecture
- [Step 5](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-5) : Testing our environment
- [Step 6](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-6) : Creating the application class
- [Step 7](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-7) : Starting the app in our index file
- [Step 8](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-8) : Getting started with controllers
- [Step 9](https://gitlab.com/cfstcyr/tutorial-soa-server-application/-/tree/step-9) : Creating the game workflow
