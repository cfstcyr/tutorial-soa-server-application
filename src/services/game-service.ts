import { StatusCodes } from "http-status-codes";
import { singleton } from "tsyringe";
import { HttpException } from "../models/http-exception";
import { PlayResult } from "../models/play-result";
import { RockPaperScissors, RockPaperScissorsGameResult, RockPaperScissorsMap, RockPaperScissorsResultMap } from "../models/rock-paper-scissors";

@singleton()
export class GameService {
    play(move: string): PlayResult {
        if (!this.isValidMove(move)) throw new HttpException(StatusCodes.BAD_REQUEST, 'Invalid move');
        const opponent = this.getRandomMove();
        return {
            played: move,
            opponent,
            won: this.doesWin(move, opponent),
        };
    }

    private isValidMove(move: string): move is RockPaperScissors {
        return move === RockPaperScissors.Paper
            || move === RockPaperScissors.Rock
            || move === RockPaperScissors.Scissors;
    }

    private getRandomMove(): RockPaperScissors {
        const moves = [
            RockPaperScissors.Paper,
            RockPaperScissors.Rock,
            RockPaperScissors.Scissors,
        ];
        return moves[Math.floor(Math.random() * moves.length)];
    }
    
    private doesWin(move: RockPaperScissors, opponent: RockPaperScissors): RockPaperScissorsGameResult {
        return RockPaperScissorsResultMap[RockPaperScissorsMap[move]][RockPaperScissorsMap[opponent]];
    }
}