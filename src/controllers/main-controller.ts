import { singleton } from "tsyringe";
import express from 'express';
import { GameService } from "../services/game-service";

@singleton()
export class MainController {
    router: express.Router;

    constructor(
        private readonly gameService: GameService,
    ) {
        this.router = express.Router();
        this.configureRoutes();
    }

    private configureRoutes() {
        this.router.get('/', (req, res) => {
            res.json({ status: 'ok' });
        });

        this.router.get('/play/:move', (req, res) => {
            res.json(this.gameService.play(req.params.move));
        });
    }
}