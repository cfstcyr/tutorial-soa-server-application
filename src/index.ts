import 'reflect-metadata';
import { container } from 'tsyringe';
import { Application } from './application';

(async () => {
    const PORT = process.env.PORT ?? 3000;
    const application = container.resolve(Application);
    
    application.app.listen(PORT, () => console.log(`Server up on port ${PORT} (http://127.0.0.1:${PORT})`));
})();