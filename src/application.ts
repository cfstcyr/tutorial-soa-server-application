import { singleton } from 'tsyringe';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import { HttpException } from './models/http-exception';
import { StatusCodes } from 'http-status-codes';
import { MainController } from './controllers/main-controller';

@singleton()
export class Application {
    readonly app: express.Application;

    constructor(
        private readonly mainController: MainController,
    ) {
        this.app = express();
        this.configure();
        this.bindRoutes();
        this.errorHandler();
    }

    private configure() {
        this.app.use(morgan('dev'));

        this.app.use(cors({
            origin: process.env.CLIENT_URL ?? '*',
            credentials: true,
        }));
    }

    private bindRoutes() {
        this.app.use('/', this.mainController.router);
    }

    private errorHandler() {
        this.app.use((
            err: HttpException | Error,
            req: express.Request,
            res: express.Response,
            next: express.NextFunction,
        ) => {
            if (err instanceof HttpException) {
                res.status(err.status);
            } else {
                res.status(StatusCodes.INTERNAL_SERVER_ERROR);
            }
            console.log(err);
            res.send(err.message);
        });
    }
}