import { RockPaperScissors, RockPaperScissorsGameResult } from "./rock-paper-scissors";

export interface PlayResult {
    played: RockPaperScissors,
    opponent: RockPaperScissors,
    won: RockPaperScissorsGameResult,
};