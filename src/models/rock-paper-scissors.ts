export enum RockPaperScissors {
    Rock = 'rock',
    Paper = 'paper',
    Scissors = 'scissors',
};
export enum RockPaperScissorsGameResult {
    Won = 'won',
    Lost = 'lost',
    Draw = 'draw',
};
export const RockPaperScissorsMap = {
    [RockPaperScissors.Rock]: 0,
    [RockPaperScissors.Paper]: 1,
    [RockPaperScissors.Scissors]: 2,
};
export const RockPaperScissorsResultMap = [
    // Opponent: Rock                   Opponent: Paper                     Opponent: Scissors
    [RockPaperScissorsGameResult.Draw,  RockPaperScissorsGameResult.Lost,   RockPaperScissorsGameResult.Won], // User: Rock
    [RockPaperScissorsGameResult.Won,   RockPaperScissorsGameResult.Draw,   RockPaperScissorsGameResult.Lost], // User: Paper
    [RockPaperScissorsGameResult.Lost,  RockPaperScissorsGameResult.Won,    RockPaperScissorsGameResult.Draw], // User: Scissors
];